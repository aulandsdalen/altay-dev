# OS.pl -- Altay OS information module
# returns JSON with OS information
# static data, should not be refreshed from UI

use JSON;
use strict;

my $altay_version = 0.2;
my $response = {
	'os_short' => $^O,
	'os_full' => `uname -rs`,
	'os_logo_image_link' => "/images/os-".$^O.".png",
	'altay_version_short' => $altay_version,
	'altay_version_full' => "Altay $altay_version",
	'perl_version' => "$^V",
};

print encode_json($response);