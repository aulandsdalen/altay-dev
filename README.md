# README #

Development repo for Altay server management system.

### What's in the box? ###

* Altay itself, .pl files in root of repo
* altay-utils, OSAL files, currently for FreeBSD only

### System requirements ###

* As for now, Altay runs reliably only on FreeBSD, but in near future it would be ported to support at least OS X and Linux systems

### How do I get set up? ###

Steps to run Altay on your FreeBSD box as for now include: 

* Git clone sources
* Set up Apache 2 w/ mod_perl
* Install all required Perl modules
* Point your web browser to address you've set up at step two