/*
* /osal/freebsd/ramload.c -- RAM usage module for Altay, FreeBSD version
* arguments -- fancy, for verbose output
* format -- total_ram_size used_ram used_ram_percentage 
*/
#include <sys/types.h>
#include <sys/sysctl.h>
#include <stdio.h>                          
#include <stdlib.h>                         
#include <string.h>
#include <unistd.h>                         

typedef enum {false, true} bool;

struct mem
{
	long int total_mem_size;
	long int free_pages_count;
	long int inactive_pages_count;
	long int pagesize;
	long int used_mem;
};
                                            
int main(int argc, char **argv) {           
struct mem meminfo;        
bool VERBOSE = false;
float percentage;
if(argc > 1 && !strcmp(argv[1], "fancy")) { 
	printf("Running in fancy (verbose) mode\n");
	VERBOSE = true;
}
size_t length = sizeof(meminfo.total_mem_size);              
if (sysctlbyname("hw.realmem", &meminfo.total_mem_size, &length, NULL, 0)<0) {
	printf("An error occured while getting hw.realmem\n");
    exit(1);                            
}
if (sysctlbyname("vm.stats.vm.v_free_count", &meminfo.free_pages_count, &length, NULL, 0) < 0) {
	printf("An error occured while getting vm.stats.vm.v_free_count\n");
	exit(1);
}
if (sysctlbyname("vm.stats.vm.v_inactive_count", &meminfo.inactive_pages_count, &length, NULL, 0) < 0) {
	printf("An error occured while getting vm.stats.vm.v_inactive_count\n");
	exit(1);
}
if (sysctlbyname("vm.stats.vm.v_page_size", &meminfo.pagesize, &length, NULL, 0) < 0) {
	printf("An error occured while getting pagesize\n");
	exit(1);
}
meminfo.used_mem = meminfo.total_mem_size - ((meminfo.free_pages_count*meminfo.pagesize)+(meminfo.inactive_pages_count*meminfo.pagesize)); //in bytes
percentage = ((float)meminfo.used_mem/(float)meminfo.total_mem_size)*100;                                   
	if (VERBOSE) {
		printf("Total RAM size: %ld Mbytes\n", meminfo.total_mem_size/(1024*1024));
		printf("Free pages count: %ld\n", meminfo.free_pages_count);
		printf("Inactive pages count: %ld\n", meminfo.inactive_pages_count);
		printf("Page size: %ld bytes\n", meminfo.pagesize);
		printf("Used RAM: %ld Mbytes, %.2f%%\n", meminfo.used_mem/(1024*1024), percentage);
	}
	else {
		printf("%ld,%ld,%.2f", meminfo.total_mem_size/(1024*1024), meminfo.used_mem/(1024*1024), percentage); //format -- total_ram_size used_mem used_ram_percentage
	}
}

