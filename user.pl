# USER.pl -- Altay user information module
# dynamically updated

use strict;
use JSON;

my $sessions;
$sessions = int(`who | wc -l`);
my $www_user = `whoami`;
my $response = {
	"sessions_count" => $sessions,
	"www_user" => $www_user,
};

print encode_json($response);