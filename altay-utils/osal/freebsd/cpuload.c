/*
* /osal/freebsd/cpuload.c -- CPU usage module for Altay, FreeBSD version
* arguments -- none
* format -- cpu percentage
*/
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    float a[4], b[4], loadavg;
    FILE *fp;
    char dump[50];
        fp = fopen("/usr/compat/linux/proc/stat","r");
        fscanf(fp,"%*s %f %f %f %f",&a[0],&a[1],&a[2],&a[3]);
        fclose(fp);
        usleep(500000);
        fp = fopen("/usr/compat/linux/proc/stat","r");
        fscanf(fp,"%*s %f %f %f %f",&b[0],&b[1],&b[2],&b[3]);
        fclose(fp);
        loadavg = ((b[0]+b[1]+b[2]) - (a[0]+a[1]+a[2])) / ((b[0]+b[1]+b[2]+b[3]) - (a[0]+a[1]+a[2]+a[3]));
        printf("%.2f",loadavg*100);
    return(0);
}

