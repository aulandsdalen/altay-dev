# SYS.pl -- Altay system information
# returns JSON with hardware information
# should not be updated from UI

use strict;
use JSON;
use utf8;
use BSD::Sysctl 'sysctl';

my $response = {
	'num_cpus' => sysctl('hw.ncpu'),
	'arch' => sysctl('hw.machine_arch'),
	'cpu_name' => sysctl('hw.model'),
	'phys_mem' => int(sysctl('hw.physmem')/1048576),
};
print encode_json($response);