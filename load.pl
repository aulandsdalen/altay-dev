# LOAD.pl -- Altay system load information module
# dynamycally updated

use strict;
use JSON;
use Unix::Uptime;

my ($load1, $load5, $load15) = Unix::Uptime->load();

my @ramdata = split(',', `/usr/local/bin/ramload`);
my $cpu_usage = `/usr/local/bin/cpuload`;


my $response = {
	'cpu' => "$cpu_usage",
	'ram_total' => "$ramdata[0]",
	'ram_used' => "$ramdata[2]",
	'uptime' => Unix::Uptime->uptime(),
	'load_average' => {
		'load_1' => $load1, 
		'load_5' => $load5,
		'load_15' => $load15,
		},
};

print encode_json($response);