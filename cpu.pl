use strict;
use JSON;
use utf8;
use Unix::Processors;
use BSD::Sysctl 'sysctl';
my $cpus = new Unix::Processors;
use Unix::Uptime;

my ($load1, $load5, $load15) = Unix::Uptime->load();

my $hw_data = {
	'num_cpus' => sysctl('hw.ncpu'),
	'arch' => sysctl('hw.machine_arch'),
	'cpu_name' => sysctl('hw.model'),
	'phys_mem' => int(sysctl('hw.physmem')/1048576),
	'uptime' => Unix::Uptime->uptime(),
	'load_average' => {
		'l1' => $load1, 
		'l5' => $load5,
		'l15' => $load15,
		},
};
print encode_json($hw_data);